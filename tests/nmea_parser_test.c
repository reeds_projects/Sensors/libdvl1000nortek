/*
 * File:   nortek_dvl1000.h
 * Licence: CeCILL
 * Author: Benoit ROPARS benoit.ropars@solutionsreeds.fr
 *
 * Created on 20 avril 2020, 10:43
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nortek_dvl1000.h"

/*
 * Simple C Test Suite
 */

void test1() {
    printf("nmea_parser_test test 1 - nmea format\n");
    void * datas;
    
    int ret = 0;
    
    unsigned char msg[] = "PNORBT1,BEAM=1,DATE=110916,TIME=112034.0346,DT1=55.717,DT2=-157.789,BV=0.15633,FM=0.00066,DIST=26.92,STAT=0x000FFFFF*2A";
    datas = nmea_parser(&msg[0],strlen(msg),&ret);
    if( ret != ERROR_NMEA_DELIMITER)printf("%%TEST_FAILED%% time=0 testname=test1 (nmea_parser_test) message=error nmea delimiter\n");

    unsigned char msg1[] = "$PNORBT1,BEAM=1,DATE=110916,TIME=112034.0346,DT1=55.717,DT2=-157.789,BV=0.15633,FM=0.00066,DIST=26.92,STAT=0x000FFFFF";
    datas = nmea_parser(&msg1[0],strlen(msg1),&ret);
    if(ret != ERROR_CHECKSUM_DELIMITER )printf("%%TEST_FAILED%% time=0 testname=test1 (nmea_parser_test) message=error checksum delimiter\n");

    unsigned char msg2[] = "$PNORBT1,BEAM=1,DATE=110916,TIME=112034.0346,DT1=55.717,DT2=-157.789,BV=0.15633,FM=0.00066,DIST=26.92,STAT=0x000FFFFF*3A";
    datas = nmea_parser(&msg2[0],strlen(msg2),&ret);
    if(ret != ERROR_CHECKSUM_VALUE )printf("%%TEST_FAILED%% time=0 testname=test1 (nmea_parser_test) message=error checksum value\n");

    
    unsigned char msg3[] = "$PNORBT1,BEAM=1,DATE=110916,TIME=112034.0346,DT1=55.717,DT2=-157.789,BV=0.15633,FM=0.00066,DIST=26.92,STAT=0x000FFFFF*2A"; 
    datas = nmea_parser(&msg3[0],strlen(msg3),&ret);
    if( ret != ASCII_NMEA_PNORBT1)printf("%%TEST_FAILED%% time=0 testname=test2 (nmea_parser_test) message=error nmea $PNORBT1\n");
    
    if(datas == NULL )printf("%%TEST_FAILED%% time=0 testname=test2 (nmea_parser_test) message=error result datas is null\n");

    if(((DVLformat350_351_t*)datas)->beam != 1 && ((DVLformat350_351_t*)datas)->stat != strtol("0x000FFFFF", NULL, 16))
        printf("%%TEST_FAILED%% time=0 testname=test2 (nmea_parser_test) message=error result datas is not correct\n");


}

void test2() {
    printf("nmea_parser_test test 2 - nmea $PNORBT1\n");
    //printf("%%TEST_FAILED%% time=0 testname=test2 (nmea_parser_test) message=error message sample\n");
/*
    void* datas_result = NULL;
    unsigned char msg[] = "$PNORBT1,BEAM=1,DATE=110916,TIME=112034.0346,DT1=55.717,DT2=-157.789,BV=0.15633,FM=0.00066,DIST=26.92,STAT=0x000FFFFF*2A"; 
    if(nmea_parser(&msg[0],strlen(msg),datas_result) != ASCII_NMEA_$PNORBT1)printf("%%TEST_FAILED%% time=0 testname=test2 (nmea_parser_test) message=error nmea $PNORBT1\n");
    
    if(datas_result == NULL )printf("%%TEST_FAILED%% time=0 testname=test2 (nmea_parser_test) message=error result datas is null\n");
    
    DVLformat350_351_t * datas = (DVLformat350_351_t*)datas_result;
*/
    
    //printf("%p\n",datas);
    //printf("beam %2.f \n",((DVLformat350_351_t*)datas)->dist);
    
    //printf("beam %d \n",datas->beam);
    
/*
    if(((DVLformat350_351_t*)datas)->beam != 1)printf("%%TEST_FAILED%% time=0 testname=test2 (nmea_parser_test) message=error of datas (beam data)\n");
    
*/
        
        
  
}

int main(int argc, char** argv) {
    printf("%%SUITE_STARTING%% nmea_parser_test\n");
    printf("%%SUITE_STARTED%%\n");

    printf("%%TEST_STARTED%% test1 (nmea_parser_test)\n");
    test1();
    printf("%%TEST_FINISHED%% time=0 test1 (nmea_parser_test) \n");

    printf("%%TEST_STARTED%% test2 (nmea_parser_test)\n");
    test2();
    printf("%%TEST_FINISHED%% time=0 test2 (nmea_parser_test) \n");

    printf("%%SUITE_FINISHED%% time=0\n");

    return (EXIT_SUCCESS);
}
