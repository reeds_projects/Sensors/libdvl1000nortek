/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Dvl1000.h
 * Author: admin
 *
 * Created on 03 October 2022, 09:16
 */

#ifndef DVL1000_H
#define DVL1000_H

#include <iostream>
#include <string>

#include "BaseDriver.h"
#include "nortek_dvl1000.h"


using namespace std;

class Dvl1000 : public BaseDriver{
public:
    
    
    
/**
     * Constructeur
     */
    Dvl1000(string pathLogFile, bool enableLog = true);
    /**
     * Destructeur
     */
    virtual ~Dvl1000();
        
    
    /**
     * Callback pour parser les données venant d'une communication autre que d'un fichier
     * @param trame, la trame à parser
     * @return 0 si ok 
     */
    int readOnSensor(string trame);
    
    /**
     * Callback pour parser les données venant d'un fichier de log
     * @param trame, la trame à parser
     * @return 0 si ok 
     */
    int readOnFile(string trame);
    
    /**
     * Callback pour parser les données venant d'un serveur HIL
     * @param trame, la trame à parser
     * @return 0 si ok
     */
    int readOnHILServer(string trame);
    
    /**
     * Callback lancer lorsque COMMUNICATION_TIMEOUT est retourné par waitDatas()
     */
    void timeout();

private:
    
    DVLformat358_359_t _dvl_datas;
    
   
    /**
     * Callback appeler après la connexion 
     * @return 0 si ok 
     */
    int initialize();
    
    /**
     * callback
     * Permet de faire l'initialisation avec paramètres contenus dans le fichier de configuration
     * @param params, params du fichiers
     * @return 0 si ok
     */
    int initializeWithParameters(Parameters params);
    
    /**
     * Permet de retourner les données reçus du capteur en chaine de caractère afin de faire le logging
     * @return les dernières données capteur
     */
    string getDatas();
    
    /**
     * Callback permettant d'attendre les caractères et ainsi créer la trame 
     * afin de la parser dans readOnSensor() ou readOnFile();
     * @return la trame à parser
     */
    string waitDatas();
    
    /**
     * Callback pour le nom des variables du log csv
     * @return une chaine de caractère sous la forme "Timestamp;Vbat;CBat"
     */
    virtual string logHeadersVarNames() ;

    /**
     * Callback pour l'unité des variables du log csv
     * @return une chaine de caractère sous la forme "ms;V;A "
     */
    virtual string logHeadersVarUnits();

    
    

};

#endif /* DVL1000_H */

