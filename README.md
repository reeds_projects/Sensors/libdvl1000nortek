## LICENCE

Cecill licensed code

Company: REEDS

Author: ROPARS Benoît (benoit.ropars@solutionsreeds.fr)

All codes are made available free of charge. These are codes from the manufacturer's documentation.
Libraries using this code are available under a proprietary license.We can also develop specific solutions for your needs. You can contact us via the following address: contact@solutionsreeds.fr


## MODIFICATION

- v.1.0 : First version
