/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Dvl1000.cpp
 * Author: admin
 *
 * Created on 03 October 2022, 09:16
 */

#include "Dvl1000.h"

#include <string>
#include <cmath>
#include <cstdio>
#include <chrono>
#include <map>
#include <utility>
#include <variant>


Dvl1000::Dvl1000(string pathLogFile, bool enableLog)
    : BaseDriver(pathLogFile)
{
    
    enableLogging(enableLog);
    
#ifdef __DEBUG_FLAG_
        cout << "Dvl1000 :: log file :" << pathLogFile <<" , actived :" << enableLog << std::endl;
#endif

    
}

Dvl1000::~Dvl1000() {
    disconnect();
    
}

string Dvl1000::logHeadersVarNames(){
    return "";
}


string Dvl1000::logHeadersVarUnits(){
    return "";
}

string Dvl1000::getDatas() {
    
    char * c_time;

    asprintf(&c_time,"%.3f",getLastReceptionTime());

    string output = c_time;
    output += ";";
    output += to_string(_dvl_datas.time);
    output += ";";
    output += to_string(_dvl_datas.dt1);
    output += ";";
    output += to_string(_dvl_datas.dt2);
    output += ";";
    output += to_string(_dvl_datas.vx);
    output += ";";
    output += to_string(_dvl_datas.vy);
    output += ";";
    output += to_string(_dvl_datas.vz);
    output += ";";
    output += to_string(_dvl_datas.fom);
    output += ";";
    output += to_string(_dvl_datas.d1);
    output += ";";
    output += to_string(_dvl_datas.d2);
    output += ";";
    output += to_string(_dvl_datas.d3);
    output += ";";
    output += to_string(_dvl_datas.d4);
    output += ";";
    output += to_string(_dvl_datas.batt);
    output += ";";
    output += to_string(_dvl_datas.ss);
    output += ";";
    output += to_string(_dvl_datas.press);
    output += ";";
    output += to_string(_dvl_datas.temp);
    output += ";";
    output += to_string(_dvl_datas.stat);
    
//#ifdef __DEBUG_FLAG_
//        cout << "Dvl1000 :: Print log "<< output << std::endl;
//#endif
    free(c_time);

    
    return output;
    
}



string Dvl1000::waitDatas(){
//#ifdef __DEBUG_FLAG_
    cout << "Dvl1000:: waitDatas()"<< std::endl;
//#endif
    
    return readDatasToString('\n');
  
}


int Dvl1000::readOnSensor(string trame){
#ifdef __DEBUG_FLAG_
    cout << "Dvl1000:: readOnSensor :: trame =" << trame << " , size:" << trame.size()<< std::endl;
#endif
    
    //si le log est activer on print la trame antérieur
    printToLogFile(getDatas());


    try{
        
        int status = 0;
        char *ctrame = &trame[0];
        
        DVLformat358_359_t *res = (DVLformat358_359_t *)nmea_parser(ctrame, trame.length(), &status);
        
        if (status != 0 && status != 358 && status != 359)
                return -1;
        
        _dvl_datas.time = res->time;
        _dvl_datas.dt1 = res->dt1;
        _dvl_datas.dt2 = res->dt2;
        _dvl_datas.vx = res->vx;
        _dvl_datas.vy = res->vy;
        _dvl_datas.vz = res->vz;

        _dvl_datas.fom = res->fom;

        _dvl_datas.d1 = res->d1;
        _dvl_datas.d2 = res->d2;
        _dvl_datas.d3 = res->d3;
        _dvl_datas.d4 = res->d4;

        _dvl_datas.batt = res->batt;
        _dvl_datas.ss = res->ss;
        _dvl_datas.press = res->press;
        _dvl_datas.temp = res->temp;
        _dvl_datas.stat = res->stat;

        free(res);

        
    }catch(const std::exception& e){
        std::cout << "Dvl1000::readOnSensor -> Invalid input: " << e.what() << '\n';
        return -20;
    }

}

int Dvl1000::readOnFile(string trame){
////#ifdef __DEBUG_FLAG_
//    cout << "Dvl1000 :: readOnFile :: trame : " << trame <<  endl;
////#endif
//    try{
//        // timeepoch
//        long long timeepoch = 0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            timeepoch = stoll(trame.substr(0, trame.find(" ")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        // time
//        int time = 0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            time = stoi(trame.substr(0, trame.find(" ")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//        
//        double f_range = 0.0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            f_range = stof(trame.substr(0, trame.find(";")));
//            f_range *= 100;
//            uint8_t i_range = 0;
//            switch((int)f_range) {
//                case 25:i_range =4; break;
//                case 50:i_range =6; break;
//                case 75:i_range =8; break;
//                case 100:i_range =10; break;
//                case 200:i_range =20; break;
//                case 300:i_range =30; break;
//                case 400:i_range =40; break;
//                case 500:i_range =50; break;
//                case 600:i_range =60; break;
//            }
//            _range_index = i_range;
//
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _gain = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        uint8_t LOGF = 0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            LOGF = stoi(trame.substr(0, trame.find(";")));
//            _LOGF = (LOGF/10)-1;
//
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        double absorption = 0.0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            absorption = stof(trame.substr(0, trame.find(";")));
//            _absorption = absorption*100;
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        uint8_t pulseLength = 0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            _pulseLength = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _min_range = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _calib = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _switch_delay = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        double frequency = 0.0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            frequency = stof(trame.substr(0, trame.find(";")));
//            _frequency = (((frequency*1000 - 2250)/5) + 100) ;
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _lastSonarReturn.head_id = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _lastSonarReturn.serial_status = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        uint16_t head_position = 0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            head_position = stoi(trame.substr(0, trame.find(";")));
//            head_position = head_position /0.9 +600;
//
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        double f_range2 = 0.0;
//        if (trame.substr(0, trame.find(";")) != "") {
//            f_range2 = stof(trame.substr(0, trame.find(";")));
//
//            f_range2 *= 100;
//            uint8_t i_range = 0;
//            switch((int)f_range2) {
//                case 25:i_range =4; break;
//                case 50:i_range =6; break;
//                case 75:i_range =8; break;
//                case 100:i_range =10; break;
//                case 200:i_range =20; break;
//                case 300:i_range =30; break;
//                case 400:i_range =40; break;
//                case 500:i_range =50; break;
//                case 600:i_range =60; break;
//            }
//            _lastSonarReturn.range_index = i_range;
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _lastSonarReturn.number_of_shots = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        if (trame.substr(0, trame.find(";")) != "") {
//            _lastSonarReturn.data_bytes = stoi(trame.substr(0, trame.find(";")));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        string scanline = "";
//        if (trame.substr(0, trame.find(";")) != "") {
//            scanline = trame.substr(0, trame.find(";"));
//        }
//        trame.erase(0, trame.find(";") + 1);
//
//        for(int i = 0 ; i < 400 ; i++){
//            if (scanline.substr(0, scanline.find(",")) != "") {
//                _lastSonarReturn._datas[i] = stof(scanline.substr(0, scanline.find(",")));
//            }
//            scanline.erase(0, scanline.find(",") + 1);
//        }
//
//        return 0;
//    
//    }catch(const std::exception& e){
//        std::cout << "Dvl1000::readOnFile -> Invalid input: " << e.what() << '\n';
//        return -20;
//    }
}

int Dvl1000::readOnHILServer(string trame){
////#ifdef __DEBUG_FLAG_
//    cout << "Dvl1000 :: readOnHILServer :: trame : " << trame <<  endl;
////#endif
//    
//    if(trame.size() != 800){
//        std::cout << "Dvl1000:: readOnHILServer :: Error trame size :  " << trame.size() << " != 800" << '\n';
//        return -1;
//    }
//
//    const uint8_t* datas = reinterpret_cast<const uint8_t*>(&trame[0]);
//     
//    _ping_count++;
//
//    _lastSonarReturn.ping_counter = _ping_count;
//    _lastSonarReturn.head_id = 0x10;
//    _lastSonarReturn.serial_status = 0;
//    _lastSonarReturn.head_position = 0;
//    _lastSonarReturn.head_direction = 0;
//    _lastSonarReturn.range_index = 20;
//    _lastSonarReturn.data_bytes = 800;
//    for(int j=0;j<800;j++){
//        //_lastSonarReturn._datas[j] =  datas[j];
//        cout << datas[j]<< endl;
//    }
//    
////    for(int i=0,j=0;i<_lastSonarReturn.data_bytes;i = i+2,j++){
////            _lastSonarReturn._datas[j] =  (((datas[i+1]&0xFE)>>1)<<8)| ((datas[i+1]&0x01)<<7) | (datas[i]&0x7F);
////            cout << _lastSonarReturn._datas[j]<< endl;
////        }
//        
//    return 0;
}

void Dvl1000::timeout(){
    cout << "Dvl1000 :: timeout() "<< endl;
}


int Dvl1000::initialize(){
    return 0;
}

int Dvl1000::initializeWithParameters(Parameters params){
    return BaseDriver::initializeWithParameters(params);
    
}

