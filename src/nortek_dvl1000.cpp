#include "nortek_dvl1000.h"

#include <stdio.h>
#include <stdlib.h>

unsigned short calculateChecksum(unsigned short *pData, unsigned
        short size) {
    unsigned short checksum = 0xB58C;
    unsigned short nbshorts = (size >> 1);
    int i;
    for (i = 0; i < nbshorts; i++) {
        checksum += *pData;
        size -= 2;
        pData++;
    }
    if (size > 0) {
        checksum += ((unsigned short) (*pData)) << 8;
    }
    return checksum;
}

unsigned int calculateNMEAChecksum(char *msg, size_t len) {

    unsigned int checksum = 0;
    for (int i = 0; i < len; i++) {
        printf("%c", msg[i]);
        checksum ^= (unsigned int) msg[i];
    }
    printf("\n");
    return checksum;
}

void * nmea_parser(char *msg, size_t length, int *code_result) {

    printf("msg : %s \nmsg_len : %ld\n\n", msg, length);

    if (msg[0] != '$') {
        if (code_result != 0)*code_result = ERROR_NMEA_DELIMITER;
        return NULL;
    }
    if (msg[length - 3] != '*') {
        if (code_result != 0)*code_result = ERROR_CHECKSUM_DELIMITER;
        return NULL;
    }

    int calc_checksum = calculateNMEAChecksum(&msg[1], length - 4);

    //read raw values
    char *values[128];
    values[0] = strtok(msg, ",") + 1; // nmea key(+1 to remove $

    int value_count = 0;
    while (values[value_count] != NULL) {

        //remove key of the values
        for (int i = 1; i < strlen(values[value_count]) + 1; i++) {
            if (values[value_count][i - 1] == '=') {
                values[value_count] = values[value_count] + i;
            }
        }

        printf(" °°°°°%s\n", values[value_count]); //printing each token

        value_count++;
        values[value_count] = strtok(NULL, ",");
    }

    //remove checksum
    values[value_count - 1] = strtok(values[value_count - 1], "*");

    //compare checksum
    unsigned int msg_checksum = (unsigned int) strtol(strtok(NULL, "*"), NULL, 16);
    if (msg_checksum != calc_checksum) {
        if (code_result != 0)*code_result = ERROR_CHECKSUM_VALUE;
        return NULL;
    }

    //check message type
    if (strcmp(values[0], "PNORBT0") == 0) {

        DVLformat350_351_t *datas_result = (DVLformat350_351_t*)malloc(sizeof (DVLformat350_351_t));
        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT0;

        datas_result->beam = atoi(values[1]);
        datas_result->date = atoi(values[2]);
        datas_result->time = atof(values[3]);
        datas_result->dt1 = atof(values[4]);
        datas_result->dt2 = atof(values[5]);
        datas_result->bv = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->dist = atof(values[8]);
        datas_result->stat = strtol(values[9], NULL, 16);

        return datas_result;


    } else if (strcmp(values[0], "PNORBT1") == 0) {


        DVLformat350_351_t *datas_result = (DVLformat350_351_t*)malloc(sizeof (DVLformat350_351_t));
        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT1;

        datas_result->beam = atoi(values[1]);
        datas_result->date = atoi(values[2]);
        datas_result->time = atof(values[3]);
        datas_result->dt1 = atof(values[4]);
        datas_result->dt2 = atof(values[5]);
        datas_result->bv = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->dist = atof(values[8]);
        datas_result->stat = strtol(values[9], NULL, 16);

        return datas_result;


    } else if (strcmp(values[0], "PNORBT3") == 0) {

        DVLformat354_355_t *datas_result = (DVLformat354_355_t*)malloc(sizeof (DVLformat354_355_t));
        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT3;

        datas_result->dt1 = atof(values[1]);
        datas_result->dt2 = atof(values[2]);
        datas_result->sp = atof(values[3]);
        datas_result->dir = atof(values[4]);
        datas_result->fom = atof(values[5]);
        datas_result->dist = atof(values[6]);

        return datas_result;

    } else if (strcmp(values[0], "PNORBT4") == 0) {
        DVLformat354_355_t *datas_result = (DVLformat354_355_t*)malloc(sizeof (DVLformat354_355_t));
        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT4;

        datas_result->dt1 = atof(values[1]);
        datas_result->dt2 = atof(values[2]);
        datas_result->sp = atof(values[3]);
        datas_result->dir = atof(values[4]);
        datas_result->fom = atof(values[5]);
        datas_result->dist = atof(values[6]);

        return datas_result;

    } else if (strcmp(values[0], "PNORBT6") == 0) {

        DVLformat356_357_t *datas_result = (DVLformat356_357_t*)malloc(sizeof (DVLformat356_357_t));
        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT6;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);


        return datas_result;

    } else if (strcmp(values[0], "PNORBT7") == 0) {
        DVLformat356_357_t *datas_result = (DVLformat356_357_t*)malloc(sizeof (DVLformat356_357_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT7;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);


        return datas_result;
    } else if (strcmp(values[0], "PNORBT8") == 0) {

        DVLformat358_359_t *datas_result = (DVLformat358_359_t*)malloc(sizeof (DVLformat358_359_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT8;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);
        datas_result->batt = atof(values[12]);
        datas_result->ss = atof(values[13]);
        datas_result->press = atof(values[14]);
        datas_result->temp = atof(values[15]);
        datas_result->stat = strtol(values[16], NULL, 16);

        return datas_result;


    } else if (strcmp(values[0], "PNORBT9") == 0) {

        DVLformat358_359_t *datas_result = (DVLformat358_359_t*)malloc(sizeof (DVLformat358_359_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORBT9;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);
        datas_result->batt = atof(values[12]);
        datas_result->ss = atof(values[13]);
        datas_result->press = atof(values[14]);
        datas_result->temp = atof(values[15]);
        datas_result->stat = strtol(values[16], NULL, 16);

        return datas_result;

    } else if (strcmp(values[0], "PNORWT3") == 0) {

        DVLformat404_405_t *datas_result = (DVLformat404_405_t*)malloc(sizeof (DVLformat404_405_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORWT3;

        datas_result->dt1 = atof(values[1]);
        datas_result->dt2 = atof(values[2]);
        datas_result->sp = atof(values[3]);
        datas_result->dir = atof(values[4]);
        datas_result->fom = atof(values[5]);
        datas_result->dist = atof(values[6]);

        return datas_result;

    } else if (strcmp(values[0], "PNORWT4") == 0) {
        DVLformat404_405_t *datas_result = (DVLformat404_405_t*)malloc(sizeof (DVLformat404_405_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORWT4;

        datas_result->dt1 = atof(values[1]);
        datas_result->dt2 = atof(values[2]);
        datas_result->sp = atof(values[3]);
        datas_result->dir = atof(values[4]);
        datas_result->fom = atof(values[5]);
        datas_result->dist = atof(values[6]);

        return datas_result;

    } else if (strcmp(values[0], "PNORWT6") == 0) {

        DVLformat406_407_t *datas_result = (DVLformat406_407_t*)malloc(sizeof (DVLformat406_407_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORWT6;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);

        return datas_result;

    } else if (strcmp(values[0], "PNORWT7") == 0) {

        DVLformat406_407_t *datas_result = (DVLformat406_407_t*)malloc(sizeof (DVLformat406_407_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORWT7;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);

        return datas_result;

    } else if (strcmp(values[0], "PNORWT8") == 0) {

        DVLformat408_409_t *datas_result = (DVLformat408_409_t*)malloc(sizeof (DVLformat408_409_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORWT8;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);
        datas_result->batt = atof(values[12]);
        datas_result->ss = atof(values[13]);
        datas_result->press = atof(values[14]);
        datas_result->temp = atof(values[15]);
        datas_result->stat = strtol(values[16], NULL, 16);

        return datas_result;

    } else if (strcmp(values[0], "PNORWT9") == 0) {

        DVLformat408_409_t *datas_result = (DVLformat408_409_t*)malloc(sizeof (DVLformat408_409_t));

        if (datas_result == NULL) {
            if (code_result != 0) *code_result = ERROR_TO_ALLOC_RESULT;
            return NULL;
        }

        if (code_result != 0)*code_result = ASCII_NMEA_PNORWT9;

        datas_result->time = atof(values[1]);
        datas_result->dt1 = atof(values[2]);
        datas_result->dt2 = atof(values[3]);
        datas_result->vx = atof(values[4]);
        datas_result->vy = atof(values[5]);
        datas_result->vz = atof(values[6]);
        datas_result->fom = atof(values[7]);
        datas_result->d1 = atof(values[8]);
        datas_result->d2 = atof(values[9]);
        datas_result->d3 = atof(values[10]);
        datas_result->d4 = atof(values[11]);
        datas_result->batt = atof(values[12]);
        datas_result->ss = atof(values[13]);
        datas_result->press = atof(values[14]);
        datas_result->temp = atof(values[15]);
        datas_result->stat = strtol(values[16], NULL, 16);

        return datas_result;

    } else {
        if (code_result != 0)*code_result = ERROR_NMEA_TYPE;
    }

    return NULL;

}
